# Domotic Project - Home Security
BUT3 group project

## Developers
Mathys PELLEN <mathys.pellen@etu.univ-lyon1.fr>  
Loan GOMEZ <loan.gomez@etu.univ-lyon1.fr>  
Emile PERRIER <emile.perrier-michon@etu.univ-lyon1.fr>  
Charline ANIFRANI <charline.anifrani@etu.univ-lyon1.fr>  
Najd ISMAIL <najd.ismail@etu.univ-lyon1.fr>

## Contributors
Cédric LACHARMOISE <cédric.lacharmoise@univ-lyon1.fr>  
Olivier MARTY <olivier.marty@univ-lyon1.fr>  
Sébastien DUMOULIN <sébastien.dumoulin@univ-lyon1.fr>

***

## Introduction
Ce projet de domotique vise à centraliser la gestion de plusieurs sous-systèmes au sein d'une maison intelligente en utilisant un M5Stack Core2 comme unité centrale. Notre responsabilité principale est la partie sécurité du système.Voici un aperçu général du projet :

![Illustration du projet](/doc/diagrams/Synoptique_general.PNG)

Ou encore cette architecture faite avec le langage PlantUML :

![Illustration du projet](/doc/diagrams/out/Synoptique.png)

## Cahier des Charges - Système de Sécurité Domotique

Ce cahier des charges définit les exigences pour la partie sécurité du projet de domotique utilisant un M5Stack Core2 comme unité centrale. Le système de sécurité doit surveiller les portes et fenêtres, gérer les accès via un badge et un code, contrôler l'éclairage extérieur en cas d'intrusion, activer une alarme sonore, et enregistrer les mouvements via des caméras et des détecteurs de mouvement.

### Objectifs

- Assurer la sécurité de la maison en surveillant les accès et les mouvements.
- Fournir une interface utilisateur pour la gestion et la surveillance du système de sécurité.
- Intégrer les périphériques de sécurité dans un système centralisé.
- Envoyer des notifications en cas d'événements de sécurité.

### Périmètre

Le système de sécurité doit inclure :
- Capteurs de portes et fenêtres.
- Système d'accès par badge et code.
- Éclairage extérieur en cas d'intrusion.
- Alarme sonore.
- Caméras et détecteurs de mouvement.

### Fonctionnalités

#### Portes et Fenêtres
- Détection d'ouverture et de fermeture.
- Envoi de notifications en cas d'ouverture non autorisée.

#### Badge d’entrée & Code
- Validation des badges d'accès et des codes.
- Autorisation ou refus de l'accès en fonction des informations fournies.

#### Éclairage Extérieur
- Activation de l'éclairage en cas de détection de mouvement.
- Contrôle manuel via l'interface utilisateur.

#### Alarme Sonore
- Activation en cas d'intrusion détectée.
- Désactivation manuelle via l'interface utilisateur.

#### Caméras et Détecteurs de Mouvement
- Surveillance continue et enregistrement des mouvements.
- Envoi d'alertes en cas de détection de mouvement suspect.

### Exigences Techniques

#### Matériel
- M5Stack Core2
- Capteurs de contact (porte/fenêtre)
- Caméras de surveillance
- Détecteurs de mouvement
- Système d'alarme sonore
- Module d'éclairage extérieur

#### Logiciel
- Firmware pour M5Stack Core2
- Logiciel de gestion des périphériques de sécurité
- Interface utilisateur web
- Base de données pour stocker les événements de sécurité
- Système de notifications (API Telegram)

#### Connectivité
- Connexion Wi-Fi pour la communication entre les périphériques et l'unité centrale.
- Protocole de communication sécurisé (e.g., HTTPS, MQTT) pour l'échange de données.

### Interface Utilisateur

#### Accès
- Accessible via navigateur web.
- Authentification requise pour accéder aux paramètres de sécurité.

#### Fonctionnalités
- Tableau de bord avec vue d'ensemble des événements récents.
- Section de configuration pour ajouter, supprimer, ou modifier les périphériques.
- Contrôle manuel des alarmes et de l'éclairage.
- Consultation des logs et historiques des événements de sécurité.

### Sécurité

- Cryptage des données sensibles (e.g., codes d'accès, vidéos de surveillance).

![Synoptique technique de l’installation](doc/diagrams/Synoptique_installation.PNG)

Le synoptique ci-dessus décrit les interactions entre le M5Stack Core2 et les différents périphériques de sécurité, tels que les capteurs de portes et fenêtres, le système d'accès par badge et code, l'éclairage extérieur activé en cas de détection de mouvement ou d'intrusion, l'alarme sonore, et les caméras avec détecteurs de mouvement.

## Description de la conception

### Structure des fichiers
Le projet AmbiOS est organisé en plusieurs répertoires principaux, chacun ayant un rôle spécifique :

1. **`doc`** : Contient la documentation du projet, y compris l'architecture du projet et la structure du dépôt.
   - *`diagrams`* : Contient un répertoire `out` avec des images utilisées dans la documentation et des diagrammes UML exportés. Le répertoire `src` contient les programmes PlantUML.

2. **`src`** : Le répertoire principal du code source du projet. Il se compose de sous-répertoires :
   - `M5Stack`
     - `COM_Security`
     - `IHM_Security`
     - `COM_RF`
   - `Raspberry`

3. **`test`** : Inclut des répertoires pour tester le projet.

4. **`setup.sh`** : Le script shell pour lancer le système de sécurité et démarrer le processus de collecte des données.

La structure du dépôt est conçue pour fournir une séparation claire des composants, facilitant ainsi la maintenance, le développement et le test des différentes fonctionnalités du système de sécurité.

### Structures de données
Les données sont principalement stockées dans une base de données MySQL sur le serveur (Raspberry Pi 3B). Les principaux types de données incluent les états des dispositifs (portes, fenêtres, volets, etc.), les événements détectés par les capteurs (mouvements, caméras, etc.), et les informations environnementales (météo, heure, date).

### Échange de données
Les données sont échangées entre les capteurs et la Raspberry Pi via le protocole MQTT, agrégées par le M5 Core 2 avant d'être stockées dans la base de données MongoDB. L'interface web interagit avec la base de données pour afficher les données en temps réel et historiques.<br>
<br> **JSON file**<br>
This part describes the structure of the json file used to communicate between the different components of the system. It will be used by M5Stack to store it in the database, and to ease debugging and tests. The json file is composed of the following elements :

```json
{
    "ID_module": "sensor_1",
    "timestamp": "AAAA-MM-JJ HH:MM:SS",
    "type": "normal",
    "location": "room",
    "sensor": ...,
    }
}
```
* ID_module : ID of the module sending the data (sensor_1, sensor_2, ...)
* timestamp : date and time of the measurement
* type : data retrieving mode (normal, button_dbg, sensors_dbg, scheduler_dbg...)
* location : location of the module sending the data (zone1, zone2, ...)
* sensors : information from the sensors

### Conception de l'IHM sur M5Stack

#### Structure du Code

Le code est structuré en plusieurs parties, chaque partie étant responsable d'une tâche spécifique.

##### Variables Globales et Déclarations

Les variables globales sont utilisées pour gérer l'état du menu, les options sélectionnées, le code d'accès, etc.

```cpp
// Déclaration des variables globales
int numOptions; 
int option = 0;
int selectedOption = 0;
bool stateChanged = true; 
String optionHistory = ""; 
String code = ""; 
String etape = "";
```
##### Structures et Types Enumérés

Le type énuméré `State` définit les différents états du menu. Chaque état correspond à un menu ou sous-menu spécifique.

```cpp
typedef enum State {
    MENU,
    MENU_CONFIGURATION,
    MENU_OUVERTURES,
    MENU_ALARME,
    MENU_DIGICODE,
    MENU_VALIDATION_CODE,
    MENU_SECURITE_CODE,
    MENU_CODE,
    MENU_FAIL_CODE,
} State;

State currentState = MENU;
```


##### Fonctionnement du Menu

Chaque menu est géré par une fonction spécifique qui est responsable de son affichage et de son interaction avec l'utilisateur. Les menus sont accessibles en changeant l'état dans l'énumération `State`. L'état actuel est géré dans la boucle principale (`loop`) à l'aide d'un switch-case, permettant ainsi de naviguer facilement entre les différents menus en fonction de l'état courant.

##### Fonctions d'Affichage du Menu

Chaque fonction est conçue pour afficher un menu ou un sous-menu spécifique sur l'écran tactile du M5 CORE 2. Voici des exemples de telles fonctions :

```cpp
void afficherMenu(int Option) {
    M5.Lcd.fillScreen(BLACK);
    M5.Lcd.setTextSize(2);
    M5.Lcd.setCursor(40, 50);
    M5.Lcd.print("1. Configuration");
    M5.Lcd.setCursor(40, 100);
    M5.Lcd.print("2. Ouvertures");
    M5.Lcd.setCursor(40, 150);
    M5.Lcd.print("3. Alarme");
}

void afficherDigicode(int Option) {
    M5.Lcd.fillScreen(BLACK);
    M5.Lcd.setTextSize(2);
    M5.Lcd.setCursor(40, 50);
    M5.Lcd.print("Entrez le code:");
    M5.Lcd.setCursor(40, 100);
    M5.Lcd.print(code);
}
```
#### Gestion des États dans la Boucle Principale

Dans la boucle principale (`loop`), l'utilisation d'un switch-case permet de gérer les différents menus et sous-menus en fonction de l'état actuel défini par l'énumération `State`.

```cpp
void loop() {
    switch (currentState) {
        case MENU:
            afficherMenu(option);
            break;
        case MENU_CONFIGURATION:
            afficherMenuConfig(option);
            break;
        case MENU_OUVERTURES:
            // Fonction pour afficher le menu des ouvertures
            break;
        case MENU_ALARME:
            // Fonction pour afficher le menu de l'alarme
            break;
        case MENU_DIGICODE:
            afficherDigicode(option);
            break;
        // Ajouter d'autres cas pour les autres états
    }
}
```
Ce switch-case permet de sélectionner la fonction d'affichage appropriée en fonction de la valeur de currentState. Ainsi, à chaque itération de la boucle principale, le programme affiche le menu correspondant à l'état courant. Cela assure une navigation fluide entre les différentes fonctionnalités du système domotique sur le M5 CORE 2.

#### Gestion des Boutons

Les boutons sont gérés efficacement à l'aide de tâches FreeRTOS et d'interruptions pour détecter les événements de bouton.

##### Tâche de Vérification des Boutons

La tâche `checkButtonTask` est responsable de surveiller en continu l'état des boutons et de notifier la tâche `handleButtonEventTask` en cas de pression détectée.

```cpp
void checkButtonTask(void *pvParameters) {
    while (1) {
        if (M5.BtnA.wasPressed()) {
            xTaskNotify(handleButtonEventTaskHandle, 1, eSetValueWithOverwrite);
        }
        if (M5.BtnB.wasPressed()) {
            xTaskNotify(handleButtonEventTaskHandle, 2, eSetValueWithOverwrite);
        }
        if (M5.BtnC.wasPressed()) {
            xTaskNotify(handleButtonEventTaskHandle, 3, eSetValueWithOverwrite);
        }
        M5.update();
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
}
```
Dans cette tâche, chaque bouton est surveillé à l'aide de wasPressed() pour détecter une pression. Lorsqu'une pression est détectée sur l'un des boutons (A, B, ou C), un message est envoyé à handleButtonEventTask via xTaskNotify, en passant un numéro d'événement correspondant au bouton pressé.

#### Tâche de Gestion des Événements de Bouton

La tâche `handleButtonEventTask` attend les notifications de `checkButtonTask` et exécute la logique appropriée en fonction du bouton pressé.

```cpp
void handleButtonEventTask(void *pvParameters) {
    uint32_t buttonEvent;
    while (1) {
        xTaskNotifyWait(0x00, ULONG_MAX, &buttonEvent, portMAX_DELAY);
        switch (buttonEvent) {
            case 1:
                // Logique pour le bouton A
                break;
            case 2:
                // Logique pour le bouton B
                break;
            case 3:
                // Logique pour le bouton C
                break;
        }
    }
}
```
Cette tâche utilise xTaskNotifyWait pour attendre une notification et, lorsqu'elle en reçoit une, elle utilise un switch-case pour exécuter le code correspondant à l'événement du bouton détecté (A, B, ou C). Cela permet une gestion réactive et efficace des interactions utilisateur via les boutons sur le M5 CORE 2.

#### Algorithmes et Logique
Le menu utilise une logique de changement d'état pour naviguer entre les différentes options et sous-menus. Les boutons A, B, et C permettent de naviguer et sélectionner les options.

##### Fonctionnalités Clés
- Validation du Code : Un sous-menu permet de vérifier et changer le code d'accès.
- Digicode : Un digicode est affiché pour entrer le code d'accès.
- Retour et Annulation : Les utilisateurs peuvent retourner au menu précédent ou annuler une action en cours.

Vous pouvez retrouver le détail de la partie Transmission ASK en 433 MHz dans le fichier README correspondant en cliquant sur ce [lien](README_Transmission.md).

Vous pouvez voir la description détaillée du code correspondant à la communciation RF du M5Stack, la mise en oeuvre du digicode et à la communication grâce à l'API Telegram dans le fichier README correspondant en cliquant sur ce [lien](README_detail.md).

## Notice d'installation

### Partie interface web
- **Serveur** : Un appareil qui reste allumé en permanence pour héberger le serveur.
- **Mosquitto Broker** : Installez le broker MQTT.
  ```console
  $ sudo apt-get install mosquitto mosquitto-clients
- **NodeRED** : Installez NodeRED.
```console
$ sudo npm install -g --unsafe-perm node-red
```
- **MongoDB** - You can install MongoDB Community Edition on the server following the instructions on the MongoDB website (https://www.mongodb.com/docs/manual/administration/install-community/). You can then access the MongoDB interface on the server at http://localhost:27017.

#### Partie API Telegram

- Connexion Wi-Fi stable
- Application Telegram installée
- Ordinateur pour la gestion du système

#### Partie IHM
- Matériel : M5 CORE 2
- Logiciel : Arduino IDE, Bibliothèques M5Stack

### Getting Started
Une fois que vous avez installé toutes les dépendances, vous pouvez exécuter le système. Pour ce faire, suivez les étapes suivantes :

#### Raspberry Pi
1. Connectez la Raspberry Pi au WiFi.
2. Clonez le dépôt sur la Raspberry Pi :
```console
$ git clone https://forge.univ-lyon1.fr/p2102635/security.git
```
3. Allez dans le dossier du dépôt :
```console
$ cd ~/path/to/security
```
(remplacez *path/to* par le chemin qui accède au dossier du dépôt)

4. Exécutez cette commande dans un terminal :
```console
$ bash ~/path/to/setup.sh
```
Cela lancera le fichier setup.sh pour installer et configurer la connexion MQTT, la base de données, etc.
(remplacez *path/to* par le chemin qui accède au script bash)

5. Modifiez le script bash pour définir l'adresse IP du serveur. Vous pouvez également modifier le sujet MQTT, l'intervalle de temps pour récupérer les données, le chemin de la socket Unix ou le chemin du fichier JSON si nécessaire.

6. Dans votre interface NodeRED (http://localhost:1880), importez le fichier Security_NodeRED.json dans le dossier NodeRED du dépôt. Cela créera le flux NodeRED pour le système comme illustré ci-dessous :

![Web Interface](doc/Flows.jpg)

7. Dans votre interface MongoDB (http://localhost:27017), créez une base de données nommée Security et une collection nommée Security.
8. Configurez le nœud Security MQTT dans le flux NodeRED avec l'adresse IP et le port de votre serveur.
9.  Redémarrez votre Raspberry Pi pour appliquer les modifications et lancer le système :
```console
$ sudo reboot
```
1.   Vous pouvez maintenant accéder à l'interface web sur le serveur à http://localhost:1880/ui. Elle affichera les données récupérées du M5Stack en temps réel ci-dessous :<br>
**Page de login**
![Web Interface](doc/Login_r.JPG)
**Page principale**
![Web Interface](doc/Home.JPG)
**Page des paramètres**
![Web Interface](doc/Settings.JPG)

#### Partie API Telegram

1. Brancher l’unité centrale avec le câble et l’adaptateur secteur fournis.
2. Suivre les étapes de démarrage affichées sur l’écran du M5 pour la configuration du Wi-Fi.
3. Créer un Bot Telegram via l’assistant de création de bot de l’application.
4. Renseigner l’ID de la conversation.
5. Se connecter à la page d’administration et changer les ID par défaut.

#### Partie IHM
1.Installer l'Arduino IDE
- Télécharger et installer l'Arduino IDE depuis le site officiel.

2.Installer les Bibliothèques
- Ouvrir l'Arduino IDE.
- Aller à Sketch -> Include Library -> Manage Libraries....
- Rechercher et installer les bibliothèques suivantes :
	- M5Stack
	- FreeRTOS
3.Téléverser le Code
- Connecter le M5 CORE 2 à votre ordinateur via un câble USB.
- Ouvrir le fichier .ino dans l'Arduino IDE.
- Sélectionner la carte M5Stack-Core2 et le bon port COM.
- Cliquer sur le bouton Upload pour téléverser le code sur la carte.

## Maintenance (partie IHM sur M5Stack)
### Mise à Jour du Code
Pour mettre à jour le code, suivez ces étapes :

1.Modifier le Code
	-Apporter les modifications nécessaires au fichier .ino.

2.Téléverser les Modifications
	-Suivre les mêmes étapes que pour l'installation pour téléverser les modifications sur le M5 CORE 2.

### Résolution des Problèmes
1.Problèmes de Connexion
	-Vérifier le câble USB et le port COM dans l'Arduino IDE.

2.Problèmes de Boutons
	-S'assurer que les boutons fonctionnent correctement et qu'ils sont correctement initialisés dans le code.

### Connexion Série et Fichier de Log
Grâce à la connexion série, il est possible de surveiller tout ce qui se passe sur la carte. Cette fonctionnalité sera utile pour déboguer et suivre l'état du système en temps réel. À l'avenir, cette sortie série pourra être utilisée pour créer un fichier log.

Exemple de Code pour la Connexion Série :
```cpp
void setup() {
    Serial.begin(115200);
    M5.begin();
    // Autres initialisations
}

void loop() {
    // Affichage du menu et gestion des états
    switch (currentState) {
        case MENU:
            Serial.println("Affichage du menu principal");
            afficherMenu(option);
            break;
        case MENU_CONFIGURATION:
            Serial.println("Affichage du menu de configuration");
            afficherMenuConfig(option);
            break;
        case MENU_OUVERTURES:
            Serial.println("Affichage du menu des ouvertures");
            // Fonction pour afficher le menu des ouvertures
            break;
        case MENU_ALARME:
            Serial.println("Affichage du menu de l'alarme");
            // Fonction pour afficher le menu de l'alarme
            break;
        case MENU_DIGICODE:
            Serial.println("Affichage du digicode");
            afficherDigicode(option);
            break;
        // Ajouter d'autres cas pour les autres états
    }

    // Autres logiques de la boucle principale
}
```
### Sauvegarde des Données
Il est recommandé de sauvegarder régulièrement votre code et vos configurations pour éviter toute perte de données.

## Tests
### Partie Interface web
1. **Tests de l'interface** :
   - Accédez à l'interface via `http://localhost:1880/`.
   - Vérifiez l'affichage et le fonctionnement de chaque élément (portes, fenêtres, volets, etc.).
   - Testez l'authentification via `http://localhost:1880/login`.

2. **Tests des fonctionnalités** :
   - Modifiez les paramètres dans la page Paramètres et vérifiez les changements.
   - Interagissez avec les éléments (par exemple, allumer et éteindre la lumière) et observez les réponses dans l'interface et les enregistrements dans MongoDB.

3. **Tests d'intégration** :
   - Assurez la réception et la transmission des données entre les capteurs, le M5 Core 2 et la Raspberry Pi via MQTT.
   - Vérifiez la mise à jour en temps réel des données sur l'interface.

### Partie IHM sur M5Stack
#### Plan de Test
1.Test d'Affichage du Menu
	- Vérifier que chaque menu s'affiche correctement et que les options sont lisibles.

2.Test de Navigation
	- S'assurer que les boutons A, B, et C permettent de naviguer entre les options et les sous-menus sans problème.

3.Test de Validation du Code
	- Tester la fonctionnalité de validation du code pour s'assurer qu'un code correct permet d'accéder aux sous-menus, et qu'un code incorrect affiche un message d'erreur.

4.Test de Changement de Code
- Vérifier que la fonctionnalité de changement de code fonctionne correctement et que le nouveau code est enregistré et utilisé pour les validations futures.


#### Procédure de Test
1.Préparer le Matériel
	- S'assurer que le M5 CORE 2 est correctement connecté et que le code est téléversé.

2.Exécuter les Tests
	- Suivre le plan de test et noter les résultats de chaque test.

3.Rapporter les Bugs
	- Documenter tout comportement inattendu ou bug rencontré pendant les tests.

#### Résultats Attendus
- Affichage Correct : Tous les menus et options doivent s'afficher correctement.
- Navigation Fluide : Les boutons doivent permettre une navigation fluide entre les menus.
- Validation Correcte : La validation du code doit fonctionner correctement pour les codes corrects et afficher des messages d'erreur pour les codes incorrects.
- Changement de Code : Le changement de code doit être possible et persister après validation.

## Mode d'emploi utilisateur
1. **Accès à l'interface** :
   - Ouvrez un navigateur web et accédez à `http://localhost:1880/login`.
   - Entrez les identifiants (par défaut : admin/admin).

2. **Utilisation de l'interface** :
   - Naviguez entre les différentes sections (Accueil, Éléments, Paramètres).
   - Sur l'onglet Éléments, visualisez l'état actuel des dispositifs et interagissez avec ceux modifiables.
   - Sur l'onglet Paramètres, visualisez les historiques sous forme de graphiques et ajustez les configurations.

3. **Surveillance et gestion** :
   - Sur l'onglet Accueil, consultez les informations du jour (heure, date, météo).
   - Utilisez les commandes disponibles pour gérer les éléments de sécurité de la maison.

4. **Modification des paramètres** :
   - Accédez à la page Paramètres et entrez les nouveaux identifiants.
   - Enregistrez les modifications et reconnectez-vous avec les nouveaux identifiants.

**Votre système est maintenant prêt à l'emploi !**

## Tâches restantes

Il reste encore quelques aspects à finaliser pour compléter le projet. Voici une liste des tâches à accomplir :

### Partie interface web
#### 1. Affichage des courbes des données
- **Description** : Intégrer l'affichage des données historiques sous forme de graphiques dans l'interface web.

#### 2. Configuration des identifiants de l'interface
- **Description** : Permettre aux utilisateurs de changer leurs identifiants de connexion depuis l'interface web.
- **Statut actuel** : Les identifiants par défaut (admin/admin) sont fonctionnels, mais la fonctionnalité de modification n'est pas encore implémentée.
- **Prochaines étapes** :
  - Ajouter un formulaire dans la page Paramètres pour permettre la modification des identifiants.

#### 3. Mise en forme agréable de l'interface
- **Description** : Améliorer l'esthétique et l'ergonomie de l'interface web pour une meilleure expérience utilisateur.
- **Statut actuel** : L'interface est fonctionnelle, mais nécessite des améliorations visuelles et ergonomiques.
- **Prochaines étapes** :
  - Utiliser des bibliothèques CSS et des frameworks front-end pour améliorer le design.
  - Ajouter des styles et des thèmes pour rendre l'interface plus attrayante visuellement.

### Partie IHM sur M5Stack
#### 1. Connexion et Dialogue avec une Interface Web
La partie IHM du projet n'est pas encore terminée. Il reste à implémenter une méthode de connexion et de dialogue avec une interface web via un protocole MQTT. Le protocole MQTT permettra une communication bidirectionnelle efficace entre le M5 CORE 2 et une plateforme web, facilitant la gestion à distance des différentes fonctionnalités domotiques.

#### 2. Récupération des Données des Capteurs
Une autre partie importante à compléter est la récupération des données des capteurs via le port série. Cette tâche repose sur la solution électronique développée par mes camarades. L'idée est d'émettre ou de recevoir les données sous forme d'octets, chaque capteur ayant un identifiant propre. Lors de l'émission, seul le capteur ayant le bon identifiant acceptera la réception des données.

Les données des capteurs seront affichées sur le M5 CORE 2 dans le menu "Ouvertures" et seront également envoyées à l'interface web. Ce mécanisme assure que toutes les informations de capteurs sont accessibles localement et à distance.

#### 3. Gestion de l'Alarme
Pour la partie alarme, il sera possible de l'activer ou de la désactiver soit depuis le M5 CORE 2, soit depuis l'interface web. Cette fonctionnalité renforcera la sécurité du système en permettant un contrôle flexible et réactif de l'alarme, selon les besoins de l'utilisateur.