## Utilisation de L’API Telegram (Loan)

Un exemple d'utilisation de l'API Telegram sur un M5 :

```cpp
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

const char* ssid = "Namaspamous";
const char* password = "azertyui";

const char* BotToken = "6944469301:AAGV3sBmdx8O3Mykad5d7VsVBFXxvS3F7EY";
const char* ChatId = "6906937868";

WiFiClient client;
unsigned long lastUpdate = 0;
const int updateInterval = 10000; // Intervalle de 5 secondes
int lastMessageId = 49; // Variable pour stocker l'ID du dernier message traité

void SendMessageTelegram(const char* message);
void ReadTelegramUpdates();

void setup() {
  M5.begin();
  Serial.begin(115200);

  // Initialise l'écran LCD
  M5.Lcd.println("Connexion au Wi-Fi...");
  
  // Connexion au Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connexion au Wi-Fi...");
  }

  // Affiche l'adresse IP
  M5.Lcd.println("Connecté au Wi-Fi");
  M5.Lcd.println("Adresse IP: ");
  M5.Lcd.print(WiFi.localIP());

  delay(1000);
  M5.Lcd.println("Acces en cours...");
  delay(1000);

  // Envoie un message de bienvenue pour indiquer que l'appareil est prêt
  SendMessageTelegram("Hello by M5 Stack");
}

void loop() {
  unsigned long currentMillis = millis();
  if (currentMillis - lastUpdate >= updateInterval) {
    lastUpdate = currentMillis;
    ReadTelegramUpdates();
  }
}

void SendMessageTelegram(const char* message) {
  HTTPClient http;
  
  String url = "https://api.telegram.org/bot";
  url += BotToken;
  url += "/sendMessage?chat_id=";
  url += ChatId;
  url += "&text=";
  url += message;

  // Démarre une connexion HTTP vers l'URL spécifiée
  http.begin(url);

  // Définit le type de contenu de la requête (ici, POST)
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");

  // Envoie la requête POST vide (vous pouvez ajouter des données si nécessaire)
  int httpResponseCode = http.POST("");

  // Vérifie le code de réponse
  if (httpResponseCode > 0) {
    Serial.print("Code de réponse : ");
    Serial.println(httpResponseCode);
    String payload = http.getString();
    Serial.println(payload);
  } else {
    Serial.print("Erreur lors de la requête : ");
    Serial.println(httpResponseCode);
  }

  // Ferme la connexion
  http.end();
}

void ReadTelegramUpdates() {
  HTTPClient http;
  String url = "https://api.telegram.org/bot";
  url += BotToken;
  url += "/getUpdates";

  // Démarre une connexion HTTP vers l'URL spécifiée
  http.begin(url);

  // Envoie la requête GET
  int httpResponseCode = http.GET();

  // Vérifie le code de réponse
  if (httpResponseCode > 0) {
    String payload = http.getString();
    Serial.println("Réponse de l'API:");
    Serial.println(payload);

    // Analyse le JSON reçu
    DynamicJsonDocument doc(2048);
    DeserializationError error = deserializeJson(doc, payload);

    if (!error) {
      JsonArray results = doc["result"];
      for (JsonObject result : results) {
        int messageId = result["message"]["message_id"];
        if (messageId > lastMessageId) {
          lastMessageId = messageId;
          const char* text = result["message"]["text"];
          if (text) {
            // Affiche le message sur le M5
            M5.Lcd.println(text);
            Serial.println(text);
          }
        }
      }
    } else {
      Serial.print("Erreur de désérialisation : ");
      Serial.println(error.c_str());
    }
  } else {
    Serial.print("Erreur lors de la requête : ");
    Serial.println(httpResponseCode);
  }

  // Ferme la connexion
  http.end();
}
```

### Description Générale
Ce script Arduino utilise un M5Stack Core2 pour se connecter à un réseau Wi-Fi, interagir avec l'API Telegram pour envoyer et recevoir des messages. Le script inclut la gestion des mises à jour Telegram pour afficher les messages sur l'écran du M5Stack.

Bibliothèques Utilisées :

WiFi.h: Pour gérer la connexion Wi-Fi.<br>HTTPClient.h: Pour effectuer des requêtes HTTP.<br>ArduinoJson.h: Pour analyser les réponses JSON de l'API Telegram.

### Constantes et Variables Globales

```cpp
const char* ssid = "Namaspamous";
const char* password = "azertyui";
const char* BotToken = "6944469301:AAGV3sBmdx8O3Mykad5d7VsVBFXxvS3F7EY";
const char* ChatId = "6906937868";
WiFiClient client;
unsigned long lastUpdate = 0;
const int updateInterval = 10000; // Intervalle de 10 secondes
int lastMessageId = 49; // ID du dernier message traité

```

- ssid et password: Informations de connexion au réseau Wi-Fi.
- BotToken et ChatId: Informations nécessaires pour interagir avec le bot Telegram.
- lastUpdate: Stocke le temps de la dernière mise à jour pour la gestion du timing.
- updateInterval: Intervalle de mise à jour en millisecondes (ici, 10 secondes).
- lastMessageId: ID du dernier message Telegram traité.

### Initialisation et Configuration

Copier le script 

```cpp
void setup() {
  M5.begin();
  Serial.begin(115200);

  // Initialise l'écran LCD
  M5.Lcd.println("Connexion au Wi-Fi...");

  // Connexion au Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connexion au Wi-Fi...");
  }

  // Affiche l'adresse IP
  M5.Lcd.println("Connecté au Wi-Fi");
  M5.Lcd.println("Adresse IP: ");
  M5.Lcd.print(WiFi.localIP());

  delay(1000);
  M5.Lcd.println("Acces en cours...");
  delay(1000);

  // Envoie un message de bienvenue pour indiquer que l'appareil est prêt
  SendMessageTelegram("Hello by M5 Stack");
}
```

1- Initialisation du M5Stack: Initialise le M5Stack et le port série.<br>
2- Connexion au Wi-Fi: Connecte l'appareil au réseau Wi-Fi en utilisant le SSID et le mot de passe fournis.<br>
3- Affichage des informations: Affiche l'adresse IP locale sur l'écran du M5Stack une fois connecté.<br>
4- Message de bienvenue: Envoie un message de bienvenue via Telegram pour indiquer que l'appareil est prêt.

### Boucle Principale

```cpp
void loop() {
  unsigned long currentMillis = millis();
  if (currentMillis - lastUpdate >= updateInterval) {
    lastUpdate = currentMillis;
    ReadTelegramUpdates();
  }
}
```

- Gestion du timing: Vérifie si l'intervalle de mise à jour est écoulé.
- Mise à jour Telegram: Appelle la fonction ReadTelegramUpdates pour vérifier les nouveaux messages.

### Fonction d'envoi de messages Telegram

```cpp
void SendMessageTelegram(const char* message) {
  HTTPClient http;

  String url = "https://api.telegram.org/bot";
  url += BotToken;
  url += "/sendMessage?chat_id=";
  url += ChatId;
  url += "&text=";
  url += message;

  // Démarre une connexion HTTP vers l'URL spécifiée
  http.begin(url);

  // Définit le type de contenu de la requête (ici, POST)
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");

  // Envoie la requête POST vide (vous pouvez ajouter des données si nécessaire)
  int httpResponseCode = http.POST("");

  // Vérifie le code de réponse
  if (httpResponseCode > 0) {
    Serial.print("Code de réponse : ");
    Serial.println(httpResponseCode);
    String payload = http.getString();
    Serial.println(payload);
  } else {
    Serial.print("Erreur lors de la requête : ");
    Serial.println(httpResponseCode);
  }

  // Ferme la connexion
  http.end();
}

```

- Construction de l'URL: Crée l'URL pour l'API Telegram en utilisant le token du bot et l'ID du chat.
- Connexion HTTP: Initialise une connexion HTTP et envoie une requête POST.
- Gestion de la réponse: Affiche le code de réponse HTTP et le payload reçu.
- Fermeture de la connexion: Termine la connexion HTTP.


### Fonction de lecture des mises à jour Telegram

```cpp

void ReadTelegramUpdates() {
  HTTPClient http;
  String url = "https://api.telegram.org/bot";
  url += BotToken;
  url += "/getUpdates";

  // Démarre une connexion HTTP vers l'URL spécifiée
  http.begin(url);

  // Envoie la requête GET
  int httpResponseCode = http.GET();

  // Vérifie le code de réponse
  if (httpResponseCode > 0) {
    String payload = http.getString();
    Serial.println("Réponse de l'API:");
    Serial.println(payload);

    // Analyse le JSON reçu
    DynamicJsonDocument doc(2048);
    DeserializationError error = deserializeJson(doc, payload);

    if (!error) {
      JsonArray results = doc["result"];
      for (JsonObject result : results) {
        int messageId = result["message"]["message_id"];
        if (messageId > lastMessageId) {
          lastMessageId = messageId;
          const char* text = result["message"]["text"];
          if (text) {
            // Affiche le message sur le M5
            M5.Lcd.println(text);
            Serial.println(text);
          }
        }
      }
    } else {
      Serial.print("Erreur de désérialisation : ");
      Serial.println(error.c_str());
    }
  } else {
    Serial.print("Erreur lors de la requête : ");
    Serial.println(httpResponseCode);
  }

  // Ferme la connexion
  http.end();
}

```

- Construction de l'URL: Crée l'URL pour obtenir les mises à jour du bot Telegram.
- Connexion HTTP: Initialise une connexion HTTP et envoie une requête GET.
- Gestion de la réponse: Affiche la réponse de l'API Telegram.
- Analyse JSON: Utilise ArduinoJson pour analyser le JSON reçu.
- Traitement des messages: Parcourt les résultats et affiche les nouveaux - messages sur l'écran du M5Stack et le port série.
- Fermeture de la connexion: Termine la connexion HTTP.

## Mise en œuvre du digicode (Loan)

```cpp
#include <M5Core2.h>

const byte ROWS = 4;  // quatre lignes
const byte COLS = 3;  // trois colonnes
char keys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};

byte rowPins[ROWS] = {22, 19, 33, 0};  
byte colPins[COLS] = {27, 32, 21};   

String input = "";

char getKey();
unsigned long lastDebounceTime = 0;
const unsigned long debounceDelay = 50;  

void setup() {
  M5.begin();
  Serial.begin(9600);
  M5.Lcd.setTextSize(3);
  M5.Lcd.setCursor(10, 10);
  M5.Lcd.println("Enter Code:");

  for (int i = 0; i < ROWS; i++) {
    pinMode(rowPins[i], OUTPUT);
    digitalWrite(rowPins[i], HIGH);  
  }

  for (int i = 0; i < COLS; i++) {
    pinMode(colPins[i], INPUT_PULLUP);  
  }
}

void loop() {
  char key = getKey();
  if (key) {
    input += key;

    M5.Lcd.fillScreen(BLACK);
    M5.Lcd.setCursor(10, 10);
    M5.Lcd.println("Enter Code:");
    M5.Lcd.setCursor(10, 50);
    M5.Lcd.println(input);
  }
}

char getKey() {
  unsigned long currentTime = millis();
  if (currentTime - lastDebounceTime > debounceDelay) {  // vérifier si délai d'anti-rebond est écoulé
    for (int r = 0; r < ROWS; r++) {
      digitalWrite(rowPins[r], LOW); 
      for (int c = 0; c < COLS; c++) {
        if (digitalRead(colPins[c]) == LOW) {
          while (digitalRead(colPins[c]) == LOW);  // Attendre que la touche soit relâchée
          digitalWrite(rowPins[r], HIGH);  
          lastDebounceTime = currentTime;  
          Serial.print("Touche détectée: ");
          Serial.println(keys[r][c]);
          return keys[r][c];
        }
      }
      digitalWrite(rowPins[r], HIGH);
    }
  }
  return '\0';  // Aucun appui détecté
}
```


### Description Générale

Ce script Arduino utilise un M5Stack Core2 pour implémenter un digicode basé sur une matrice de clavier (keypad) 4x3. Le programme lit les entrées du clavier, affiche les touches pressées sur l'écran du M5Stack, et gère l'anti-rebond pour éviter les lectures multiples lors d'une seule pression de touche.

Bibliothèques Utilisées :

M5Core2.h: Pour gérer les fonctionnalités du M5Stack Core2.


Constantes et Variables Globales :

```cpp
const byte ROWS = 4;  // quatre lignes
const byte COLS = 3;  // trois colonnes
char keys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};

byte rowPins[ROWS] = {22, 19, 33, 0};  
byte colPins[COLS] = {27, 32, 21};   

String input = "";

char getKey();
unsigned long lastDebounceTime = 0;
const unsigned long debounceDelay = 50;  

```

ROWS et COLS: Définissent la taille de la matrice du clavier.<br>
keys: Tableau contenant les caractères correspondant à chaque touche de la matrice.<br>
rowPins et colPins: Définissent les broches Arduino utilisées pour les lignes et les colonnes du clavier.<br>
input: Stocke la séquence de touches pressées.<br>
lastDebounceTime: Stocke le temps de la dernière lecture de touche pour gérer l'anti-rebond.<br>
debounceDelay: Intervalle de temps pour l'anti-rebond (50 ms).<br>

### Initialisation et Configuration

```cpp
void setup() {
  M5.begin();
  Serial.begin(9600);
  M5.Lcd.setTextSize(3);
  M5.Lcd.setCursor(10, 10);
  M5.Lcd.println("Enter Code:");

  for (int i = 0; i < ROWS; i++) {
    pinMode(rowPins[i], OUTPUT);
    digitalWrite(rowPins[i], HIGH);  
  }

  for (int i = 0; i < COLS; i++) {
    pinMode(colPins[i], INPUT_PULLUP);  
  }
}
```

Initialisation du M5Stack : Initialise le M5Stack et le port série.<br>
Affichage initial: Définit la taille du texte et affiche "Enter Code:" sur l'écran du M5Stack.<br><br>
Configuration des broches:
- Les broches des lignes (rowPins) sont configurées en sortie et initialisées à HIGH.
- Les broches des colonnes (colPins) sont configurées en entrée avec résistance pull-up.


### Boucle Principale
```cpp
void loop() {
  char key = getKey();
  if (key) {
    input += key;

    M5.Lcd.fillScreen(BLACK);
    M5.Lcd.setCursor(10, 10);
    M5.Lcd.println("Enter Code:");
    M5.Lcd.setCursor(10, 50);
    M5.Lcd.println(input);
  }
}
```

Lecture des touches: Appelle getKey pour lire la touche pressée.<br>
Mise à jour de l'affichage: Si une touche est pressée, elle est ajoutée à la variable input et l'écran est mis à jour pour afficher la séquence complète de touches pressées.


### Fonction de Lecture des Touches

```cpp
char getKey() {
  unsigned long currentTime = millis();
  if (currentTime - lastDebounceTime > debounceDelay) {  // vérifier si délai d'anti-rebond est écoulé
    for (int r = 0; r < ROWS; r++) {
      digitalWrite(rowPins[r], LOW); 
      for (int c = 0; c < COLS; c++) {
        if (digitalRead(colPins[c]) == LOW) {
          while (digitalRead(colPins[c]) == LOW);  // Attendre que la touche soit relâchée
          digitalWrite(rowPins[r], HIGH);  
          lastDebounceTime = currentTime;  
          Serial.print("Touche détectée: ");
          Serial.println(keys[r][c]);
          return keys[r][c];
        }
      }
      digitalWrite(rowPins[r], HIGH);
    }
  }
  return '\0';  // Aucun appui détecté
}
```

Gestion de l'anti-rebond: Vérifie si le délai d'anti-rebond est écoulé avant de lire les touches.<br>
Scan des lignes et des colonnes:
Met la ligne actuelle à LOW et lit les colonnes.<br>
Si une colonne est LOW, une touche est pressée.<br><br>
Attend que la touche soit relâchée (boucle while).<br>
Met à jour lastDebounceTime et retourne le caractère correspondant à la touche pressée.<br>
Retour à l'état initial: Remet la ligne à HIGH avant de passer à la suivante.<br>

### Fonctionnement du Digicode : 
Le digicode fonctionne en utilisant une matrice de clavier. Lorsque vous appuyez sur une touche, une ligne est connectée à une colonne. <br> Voici un résumé du processus :<br><br>
Initialisation des Broches:<br>
Les broches des lignes sont configurées en sorties et initialisées à HIGH.
Les broches des colonnes sont configurées en entrées avec résistances pull-up.<br><br>
Scan des Touches:<br>
La fonction getKey met une ligne à LOW.
Elle lit ensuite les broches des colonnes pour détecter si l'une d'elles est à LOW.
Si une colonne est à LOW, cela signifie qu'une touche de la matrice (intersection de la ligne et de la colonne) est pressée.<br><br>
Détection et Traitement des Touches:<br>
La touche est détectée et son caractère correspondant est retourné.
Le script attend que la touche soit relâchée avant de continuer (anti-rebond).
L'affichage est mis à jour pour montrer la séquence de touches pressées.<br><br>
Anti-Rebond:<br>
Pour éviter les lectures multiples lors d'une seule pression de touche, un délai d'anti-rebond de 50 ms est utilisé.
lastDebounceTime est mis à jour à chaque détection de touche pour gérer ce délai.
Ce mécanisme permet de lire de manière fiable les entrées d'un clavier matriciel et d'afficher les séquences de touches pressées sur l'écran du M5Stack.


## Communication RF (Loan)

Bien sûr, je vais détailler davantage le fonctionnement des fonctions et expliquer votre choix du baud rate et de la procédure de réveil du récepteur.

### Explication du Script d'Émission (SEND)

```cpp
#include <M5Core2.h>

#define TX_PIN 14  	// Pin de transmission
#define STX 0x02   	// Début du texte
#define ETX 0x03   	// Fin du texte

HardwareSerial mySerial(2); // Utilisation du port série 2

void sendData(const String &data);

const char* messages[] = {"JE-SUIS-t0i", "TATA", "TUTU", "TITI"};
const int numMessages = sizeof(messages) / sizeof(messages[0]);
int currentIndex = 0;

void setup() {
  M5.begin();  // Initialisation de M5Core2
  mySerial.begin(300, SERIAL_8N1, -1, TX_PIN); // Configuration de la communication série à 300 bps sur TX_PIN
  M5.Lcd.setTextSize(3);  // Définition de la taille du texte sur l'écran
  M5.Lcd.setCursor(10, 10); // Position du curseur sur l'écran
  M5.Lcd.println("INIT");   // Affichage du message INIT sur l'écran
  delay(1000); // Attente de 1 seconde pour s'assurer que la communication série est établie
}

void loop() {
  M5.Lcd.fillScreen(TFT_BLACK); // Effacement de l'écran
  M5.Lcd.setCursor(10, 10); // Position du curseur sur l'écran
  M5.Lcd.println("ENVOI"); // Affichage du message ENVOI sur l'écran
  sendData(messages[currentIndex]); // Appel de la fonction pour envoyer les données du tableau messages
  currentIndex = (currentIndex + 1) % numMessages; // Passage au message suivant, retour au début si nécessaire
  delay(2000); // Attente de 2 secondes avant d'envoyer le prochain message
}

void sendData(const String &data) {
  pinMode(TX_PIN, OUTPUT); // Configuration du TX_PIN comme sortie
  digitalWrite(TX_PIN, LOW); // Mise à l'état bas de TX_PIN pendant 100 ms
  delay(100);
  digitalWrite(TX_PIN, HIGH); // Mise à l'état haut pour permettre la transmission
  delay(10); // Petit délai pour assurer l'établissement de l'état haut

  mySerial.end(); // Arrêt de la communication série avant la réinitialisation
  mySerial.begin(300, SERIAL_8N1, -1, TX_PIN); // Réinitialisation de la communication série à 300 bps sur TX_PIN
 
  mySerial.write(STX); // Envoi du marqueur de début
  mySerial.print(data); // Envoi des données
  mySerial.write(ETX); // Envoi du marqueur de fin
}
```

### Fonctionnement du Script d'Émission (SEND)

1. **Initialisation (`setup()`)** :
   - **`M5.begin()`** : Initialise le M5Core2 pour l'utilisation.
   - **`mySerial.begin(300, SERIAL_8N1, -1, TX_PIN)`** : Initialise le port série 2 à une vitesse de 300 bauds, avec 8 bits de données, pas de parité et 1 bit d'arrêt, sur le pin de transmission `TX_PIN`.
   - **Affichage sur l'écran** : Utilise l'écran du M5Core2 pour afficher "INIT" pendant 1 seconde.

2. **Fonction `sendData(const String &data)`** :
   - **Préparation de l'envoi** :
 	- Configure le `TX_PIN` en sortie et envoie un signal bas (`LOW`) pendant 100 ms, suivi d'un signal haut (`HIGH`) pour permettre la transmission. Cela assure que le récepteur est réveillé et prêt à recevoir les données.
   
   - **Réinitialisation de la communication série** :
 	- **`mySerial.end()`** : Arrête la communication série pour permettre une réinitialisation propre.
 	- **`mySerial.begin(300, SERIAL_8N1, -1, TX_PIN)`** : Réinitialise la communication série avec les mêmes paramètres pour garantir la synchronisation avec le récepteur.

   - **Envoi des données** :
 	- **`mySerial.write(STX)`** : Envoie le marqueur de début (`STX`) pour indiquer le début des données.
 	- **`mySerial.print(data)`** : Envoie les données (`data`) en tant que texte.
 	- **`mySerial.write(ETX)`** : Envoie le marqueur de fin (`ETX`) pour indiquer la fin des données.

   Cette séquence assure que les données sont correctement encadrées (`STX` et `ETX`) pour que le récepteur puisse identifier les débuts et fins des trames de données.

### Explication du Script de Réception (RECEIVE)

```cpp
#include <M5Core2.h>

#define RX_PIN 13  	// Pin de réception
#define STX 0x02   	// Début du texte
#define ETX 0x03   	// Fin du texte

HardwareSerial mySerial(2); // Utilisation du port série 2

void processData(const String &data);

void setup() {
  M5.begin(); // Initialisation de M5Core2
  mySerial.begin(300, SERIAL_8N1, RX_PIN, -1); // Configuration de la communication série à 300 bps sur RX_PIN
  Serial.begin(9600); // Initialisation de la communication série pour le débogage
  M5.Lcd.setTextSize(3); // Définition de la taille du texte sur l'écran
  M5.Lcd.setCursor(10, 10); // Position du curseur sur l'écran
  M5.Lcd.println("Prêt à recevoir des données..."); // Affichage du message sur l'écran
}

void loop() {
  static bool receiving = false; // Initialisation de la réception des données à faux
  static String receivedData = ""; // Initialisation des données reçues à une chaîne vide

  while (mySerial.available() > 0) {
	char incomingByte = mySerial.read(); // Lecture du byte entrant
    
	if (incomingByte == STX) {
  	receiving = true; // Passage de la réception à vrai
  	receivedData = ""; // Réinitialisation des données reçues
	} else if (incomingByte == ETX) {
  	receiving = false; // Passage de la réception à faux
  	processData(receivedData); // Traitement des données reçues
	} else {
  	if (receiving) {
    	receivedData += incomingByte; // Ajout du byte reçu aux données
  	}
	}
  }
}

void processData(const String &data) {
  Serial.print("Données reçues: "); // Affichage du message sur le port série
  Serial.println(data); // Affichage des données reçues sur le port série
  M5.Lcd.fillScreen(TFT_BLACK); // Effacement de l'écran avant d'afficher les nouvelles données
  M5.Lcd.setCursor(10, 40); // Position du curseur sur l'écran
  M5.Lcd.setTextColor(TFT_WHITE); // Définition de la couleur du texte sur l'écran
  M5.Lcd.println(data); // Affichage des données reçues sur l'écran
}
```

### Fonctionnement du Script de Réception (RECEIVE)

1. **Initialisation (`setup()`)** :
   - **`M5.begin()`** : Initialise le M5Core2 pour l'utilisation.
   - **`mySerial.begin(300, SERIAL_8N1, RX_PIN, -1)`** : Initialise le port série 2 à une vitesse de 300 bauds, avec 8 bits de données, pas de parité et 1 bit d'arrêt, sur le pin de réception `RX_PIN`.
   - **Affichage sur l'écran** : Utilise l'écran du M5Core2 pour afficher "Prêt à recevoir des données..." pour indiquer que le récepteur est prêt à recevoir.

2. **Fonction `processData(const String &data)`** :
   - **Réception des données** :
 	- **`mySerial.available()`** : Vérifie la disponibilité de données sur le port série.
 	- **`mySerial.read()`** : Lit chaque byte entrant sur le port série.
   
   - **Détection des marqueurs** :
 	- **`STX`** : Marqueur de début de texte. Lorsque ce marqueur est détecté, le récepteur commence à collecter les données.
 	- **`ETX`** : Marqueur de fin de texte. Lorsque ce marqueur est détecté, le récepteur arrête de collecter les données et les passe à la fonction `processData`.

   - **Traitement des données** :
 	- **`processData(receivedData)`** : Une fois que la trame complète

 (entre `STX` et `ETX`) est reçue, cette fonction est appelée pour traiter les données. Elle les affiche à la fois sur le port série et sur l'écran du M5Core2 après avoir effacé l'écran pour afficher les nouvelles données.

### Choix du Baud Rate et du Réveil du Récepteur

- **Baud Rate (300 bauds)** : Vous avez choisi une vitesse de communication de 300 bauds (bits par seconde). Ce choix est basé sur votre étude préliminaire du module RF, où vous avez observé que le signal capté montrait une distorsion à des fréquences plus élevées. Vous avez ajusté la fréquence pour atteindre un baud rate stable de 300 bauds, où le signal était plus fiable et présentait le même duty cycle en sortie.

- **Réveil du Récepteur** : Avant d'envoyer les données, vous abaissez le TX_PIN pendant 100 ms pour "réveiller" le récepteur. Cela permet de s'assurer que le récepteur est actif et prêt à recevoir les données. Sans cette étape, une partie de la trame pourrait être tronquée si le récepteur n'est pas prêt à recevoir immédiatement après le début de la transmission.

Ces ajustements sont cruciaux pour assurer une communication série fiable entre votre émetteur et votre récepteur, en tenant compte des spécificités de votre configuration matérielle et des caractéristiques du module RF que vous utilisez.





