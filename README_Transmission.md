## Rapport sur la Transmission en ASK à 433 MHz 
### Introduction 
Dans le cadre de notre projet de groupe, j'ai été chargé de concevoir et de mettre en œuvre la partie RF pour la transmission en Amplitude Shift Keying (ASK) à 433 MHz. Cette technologie est largement utilisée pour des applications sans fil à courte portée en raison de ses avantages en termes de pénétration et de compatibilité avec une grande variété de composants RF. Ce rapport détaille les choix techniques effectués et justifie leur pertinence pour le projet. 
### Modulation ASK 
#### Pourquoi la modulation ASK ? 
- **Simplicité** : ASK est l'une des formes de modulation les plus simples à mettre en œuvre, ce qui réduit la complexité de la conception. 

- **Coût** : Les composants nécessaires pour une modulation ASK sont généralement moins chers, ce qui permet de maintenir les coûts bas. 

- **Applications adaptées** : ASK est adéquat pour les transmissions de données simples à des débits modérés, typiques des télécommandes, capteurs sans fil et autres applications similaires. 
### Fréquence de 433 MHz 
#### Avantages de 433 MHz : 
- **Bonne pénétration** : Cette fréquence permet une bonne pénétration à travers les murs et autres obstacles, essentielle pour les communications intérieures. 

- **Large disponibilité des composants** : La fréquence de 433 MHz est bien supportée par un large éventail de composants standard, facilitant la conception. 

- **Conformité réglementaire** : La bande de fréquence de 433 MHz est généralement libre d'utilisation pour des applications industrielles, scientifiques et médicales (ISM), sans nécessiter de licence dans de nombreux pays. 

### Utilisation d'un Commutateur SPDT à Haute Isolation 
Pour une efficacité optimale, nous avons décidé d'utiliser un seul ensemble d'antennes pour l'émission et la réception des signaux, en implémentant un commutateur SPDT (Single Pole Double Throw) à haute isolation utilisant des diodes PIN. 

#### Pourquoi un commutateur SPDT avec diodes PIN ? 
- **Haute isolation** : Empêche l'interférence entre les signaux émis et reçus, crucial pour maintenir la qualité du signal. 

- **Diodes PIN** : Ces diodes sont capables de gérer des niveaux de puissance élevés et de commuter rapidement entre les modes d'émission et de réception. 

- **Efficacité** : Réduit la complexité du circuit en nécessitant moins de composants supplémentaires, ce qui minimise les pertes d'insertion. 

### Développement du Projet 
Au début du projet, nous avons développé une version séparée avec l'émetteur et le récepteur chacun sur une carte distincte. Cette approche initiale a permis de tester et de valider les fonctionnalités de base des deux modules individuellement. 

Actuellement, nous travaillons à l'intégration de l'émetteur et du récepteur sur une même carte. Cette étape vise à réduire la taille globale du système et à améliorer l'efficacité en partageant certains composants entre les deux modules. 

L'émetteur et le récepteur sont développés en se basant sur un module RF à 433 MHz, avec un processus de rétro-ingénierie pour comprendre et reproduire ses caractéristiques. 

### Comparaison avec d'Autres Options 
#### Modulations Alternatives : PSK et FSK 
- **PSK (Phase Shift Keying)** 

    - **Avantages** : Plus résistant aux interférences et au bruit que ASK, utilise la bande passante de manière plus efficace. 

    - **Inconvénients** : Nécessite des circuits de modulation et de démodulation plus complexes, composants plus coûteux. 

- **FSK (Frequency Shift Keying)** 

    - **Avantages** : Très résistant aux interférences de puissance et aux variations de signal, efficace pour des applications nécessitant des transmissions fiables. 

    - **Inconvénients** : Implémentation et démodulation plus complexes que ASK, peut nécessiter plus de bande passante. 

#### Commutateurs SPDT Alternatifs 
- **Diodes PIN en série uniquement** 

    - **Avantages** : Configuration plus simple et facile à implémenter, moins de pertes d'insertion. 

    - **Inconvénients** : Peut ne pas offrir une isolation suffisante pour certaines applications sensibles. 

- **Diodes PIN en shunt uniquement** 

    - **Avantages** : Offre une meilleure isolation que les configurations en série, efficace pour les applications nécessitant une bonne isolation RF. 

    - **Inconvénients** : Peut introduire plus de pertes d'insertion. 

### Conclusion 
En résumé, bien que d'autres techniques de modulation comme PSK et FSK offrent des avantages en termes de robustesse et d'efficacité spectrale, la modulation ASK reste la plus adaptée pour notre application en raison de sa simplicité et de son coût réduit. De même, le choix d'un commutateur SPDT à haute isolation utilisant des diodes PIN en série et en shunt assure une performance optimale, bien que des configurations plus simples puissent être envisagées pour des applications moins exigeantes. 

 


 