# UPDATE
sudo apt update
##########

# MOSQUITTO
sudo apt install -y mosquitto mosquitto-clients
echo "listener 1883" >> /etc/mosquitto/mosquitto.conf
echo "allow_anonymous true" >> /etc/mosquitto/mosquitto.conf
sudo systemctl start mosquitto
##########

# NODE-RED
sudo apt install build-essential git curl
bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered)
#sudo npm install -g --unsafe-perm node-red
##########

# DATABASE (MySQL)
sudo apt install -y mysql-server

# Configuration sécurisée de MySQL
sudo mysql_secure_installation

# Vous pouvez répondre aux questions interactives avec des réponses appropriées, par exemple en définissant un mot de passe root pour MySQL, en supprimant les utilisateurs anonymes, etc.
# Il est recommandé de sécuriser votre installation MySQL.

# Vous pouvez également créer une base de données et un utilisateur dédiés pour votre application, par exemple :

sudo mysql -u root -p <<EOF
CREATE DATABASE db;
CREATE USER 'root'@'localhost' IDENTIFIED BY 'root';
GRANT ALL PRIVILEGES ON db.* TO 'root'@'localhost';
FLUSH PRIVILEGES;
EOF
