/*
#include <M5Core2.h>

#define TX_PIN 14
#define STX 0x02 // Début du texte
#define ETX 0x03 // Fin du texte

HardwareSerial mySerial(2); // Utilisation du port série 2

void sendData(const String &data);

const char* messages[] = {"JE-SUIS-t0i", "TATA", "TUTU", "TITI"};
const int numMessages = sizeof(messages) / sizeof(messages[0]);
int currentIndex = 0;

void setup() {
  M5.begin();
  mySerial.begin(300, SERIAL_8N1, -1, TX_PIN); // Configurer la communication série à 300 bps sur TX_PIN
  M5.Lcd.setTextSize(3);
  M5.Lcd.setCursor(10, 10);
  M5.Lcd.println("INIT");
  delay(1000); // Attendre pour s'assurer que la communication série est établie
}

void loop() {
  M5.Lcd.fillScreen(TFT_BLACK);
  M5.Lcd.setCursor(10, 10);
  M5.Lcd.println("ENVOI");
  sendData(messages[currentIndex]);
  currentIndex = (currentIndex + 1) % numMessages; // Passer au message suivant, revenir au début si nécessaire
  delay(2000); // Attendre 2 secondes avant d'envoyer à nouveau
}

void sendData(const String &data) {
  // Mettre la broche TX à l'état bas pendant 100 ms
  pinMode(TX_PIN, OUTPUT); // Configurer TX_PIN comme sortie
  digitalWrite(TX_PIN, LOW);
  delay(100);
  // Mettre la broche TX à l'état haut pour permettre la transmission
  digitalWrite(TX_PIN, HIGH);
  delay(10); // Petit délai pour s'assurer que l'état haut est établi
  
  // Rétablir la communication série
  mySerial.end(); // Arrêter la communication série avant de la réinitialiser
  mySerial.begin(300, SERIAL_8N1, -1, TX_PIN); 
  
  mySerial.write(STX); // Envoyer le marqueur de début
  mySerial.print(data); // Envoyer les données
  mySerial.write(ETX); // Envoyer le marqueur de fin
}

*/

#include <M5Core2.h>

#define RX_PIN 13
#define STX 0x02 // Début du texte
#define ETX 0x03 // Fin du texte

HardwareSerial mySerial(2); // Utilisation du port série 2

void processData(const String &data);

void setup() {
  M5.begin();
  mySerial.begin(300, SERIAL_8N1, RX_PIN, -1); // Configurer la communication série à 1200 bps sur RX_PIN
  Serial.begin(9600); // Initialiser la communication série pour le débogage
  M5.Lcd.setTextSize(3);
  M5.Lcd.setCursor(10, 10);
  M5.Lcd.println("Prêt à recevoir des données...");
}

void loop() {
  static bool receiving = false;
  static String receivedData = "";

  while (mySerial.available() > 0) {
    char incomingByte = mySerial.read();
    
    if (incomingByte == STX) {
      receiving = true;
      receivedData = ""; // Réinitialiser les données reçues
    } else if (incomingByte == ETX) {
      receiving = false;
      processData(receivedData); // Traiter les données reçues
    } else {
      if (receiving) {
        receivedData += incomingByte; // Ajouter le byte reçu aux données
      }
    }
  }
}

void processData(const String &data) {
  Serial.print("Données reçues: ");
  Serial.println(data);
  M5.Lcd.fillScreen(TFT_BLACK); // Effacer l'écran avant d'afficher les nouvelles données
  M5.Lcd.setCursor(10, 40);
  M5.Lcd.setTextColor(TFT_WHITE); // Définir la couleur du texte
  M5.Lcd.println(data);
}


// la pin de gauche de l'emmeteur ( le petit ) est les data
// celle du milieu vcc
// celle de droite gnd

// pour le recever la pin de gauche et vcc et celle de droite le gnd 
// les deux du milieu sont les meme et sont du data 

// Pour une bonne transmisison un duty cycle de maximum 90%
// 1.5V minimum pour le signal d'entre 
// Auccun soucicis jusqua 20V
// Minmum 10Hz
// Maimum 14 Khz 
// ideal 2kHz
