#include <M5Core2.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

const char* ssid = "Namaspamous";
const char* password = "azertyui";

const char* BotToken = "6944469301:AAGV3sBmdx8O3Mykad5d7VsVBFXxvS3F7EY";
const char* ChatId = "6906937868";

WiFiClient client;
unsigned long lastUpdate = 0;
const int updateInterval = 10000; // Intervalle de 5 secondes
int lastMessageId = 49; // Variable pour stocker l'ID du dernier message traité

void SendMessageTelegram(const char* message);
void ReadTelegramUpdates();

void setup() {
  M5.begin();
  Serial.begin(115200);

  // Initialise l'écran LCD
  M5.Lcd.println("Connexion au Wi-Fi...");
  
  // Connexion au Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connexion au Wi-Fi...");
  }

  // Affiche l'adresse IP
  M5.Lcd.println("Connecté au Wi-Fi");
  M5.Lcd.println("Adresse IP: ");
  M5.Lcd.print(WiFi.localIP());

  delay(1000);
  M5.Lcd.println("Acces en cours...");
  delay(1000);

  // Envoie un message de bienvenue pour indiquer que l'appareil est prêt
  SendMessageTelegram("Hello by M5 Stack");
}

void loop() {
  unsigned long currentMillis = millis();
  if (currentMillis - lastUpdate >= updateInterval) {
    lastUpdate = currentMillis;
    ReadTelegramUpdates();
  }
}

void SendMessageTelegram(const char* message) {
  HTTPClient http;
  
  String url = "https://api.telegram.org/bot";
  url += BotToken;
  url += "/sendMessage?chat_id=";
  url += ChatId;
  url += "&text=";
  url += message;

  // Démarre une connexion HTTP vers l'URL spécifiée
  http.begin(url);

  // Définit le type de contenu de la requête (ici, POST)
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");

  // Envoie la requête POST vide (vous pouvez ajouter des données si nécessaire)
  int httpResponseCode = http.POST("");

  // Vérifie le code de réponse
  if (httpResponseCode > 0) {
    Serial.print("Code de réponse : ");
    Serial.println(httpResponseCode);
    String payload = http.getString();
    Serial.println(payload);
  } else {
    Serial.print("Erreur lors de la requête : ");
    Serial.println(httpResponseCode);
  }

  // Ferme la connexion
  http.end();
}

void ReadTelegramUpdates() {
  HTTPClient http;
  String url = "https://api.telegram.org/bot";
  url += BotToken;
  url += "/getUpdates";

  // Démarre une connexion HTTP vers l'URL spécifiée
  http.begin(url);

  // Envoie la requête GET
  int httpResponseCode = http.GET();

  // Vérifie le code de réponse
  if (httpResponseCode > 0) {
    String payload = http.getString();
    Serial.println("Réponse de l'API:");
    Serial.println(payload);

    // Analyse le JSON reçu
    DynamicJsonDocument doc(2048);
    DeserializationError error = deserializeJson(doc, payload);

    if (!error) {
      JsonArray results = doc["result"];
      for (JsonObject result : results) {
        int messageId = result["message"]["message_id"];
        if (messageId > lastMessageId) {
          lastMessageId = messageId;
          const char* text = result["message"]["text"];
          if (text) {
            // Affiche le message sur le M5
            M5.Lcd.println(text);
            Serial.println(text);
          }
        }
      }
    } else {
      Serial.print("Erreur de désérialisation : ");
      Serial.println(error.c_str());
    }
  } else {
    Serial.print("Erreur lors de la requête : ");
    Serial.println(httpResponseCode);
  }

  // Ferme la connexion
  http.end();
}


// https://api.telegram.org/bot6944469301:AAGV3sBmdx8O3Mykad5d7VsVBFXxvS3F7EY/sendMessage?chat_id=6906937868&text=COUCOU
// https://api.telegram.org/bot6944469301:AAGV3sBmdx8O3Mykad5d7VsVBFXxvS3F7EY/getUpdates
//const char* BotToken = "6944469301:AAGV3sBmdx8O3Mykad5d7VsVBFXxvS3F7EY"; 



