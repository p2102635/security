#include <M5Core2.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>

QueueHandle_t buttonQueue;

// Structure pour les données du bouton
typedef struct {
    bool AwasReleased;
    bool BwasReleased;
    bool CwasReleased;
} ButtonEvent;

#define CODE_WIDTH 20
#define CODE_HEIGHT 40 
#define CODE_PADDING 10
#define NUM_COLUMNS 11
#define NUM_ROWS 1

// Déclaration des variables globales
int numOptions; 
int option = 0;
int selectedOption = 0;
int previousOption = -1; // Initialisé à une valeur différente de selectedOption pour détecter le premier changement
bool stateChanged = true; // Variable pour suivre les changements d'état
String optionHistory = ""; // Chaîne de caractères pour stocker l'historique des options sélectionnées
String code = ""; // variable pour enregistrer le code
String etape = ""; // variable pour savoir ou un se trouve

// Structure du menu
typedef enum State {
    MENU,
    MENU_CONFIGURATION,
    MENU_OUVERTURES,
    MENU_ALARME,
    MENU_QUITTER,
    MENU_DIGICODE,
    MENU_BADGE,
    MENU_VALIDATION_CODE,
    MENU_SECURITE_CODE,
    MENU_CODE,
    MENU_FAIL_CODE,
};

State currentState = MENU;

// Définition des chiffres du digicode
const char chiffres[NUM_ROWS][NUM_COLUMNS] = {
    {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'X'},
};

// Prototypes des fonctions
void afficherMenu(int selectedOption);
void afficherMenuConfig(int selectedOptionConfig);
void afficherDigicode(int option);
void afficherMenuOuvertures(int Option);
void afficherMenuAlarme(int Option);
void gestionBoutons();
void checkButtonTask(void *pvParameters);
void handleButtonEventTask(void *pvParameters);
String updateOptionHistory(int newOption);
void retirerDernierCaractere(String valeur);
void afficherMenuValidationCode(int Option);
void afficherMenuFailCode(int Option);
void afficherMenuCode(int Option) ;


void setup() {
    M5.begin();  // Initialisation de M5Core2
    M5.Lcd.setTextColor(TFT_WHITE);  // Réglage de la couleur de la police
    M5.Lcd.setBitmapColor(TFT_ORANGE, TFT_ORANGE);
    M5.Lcd.setTextSize(5);  // Réglage de la taille de la police
    M5.Lcd.setCursor(65, 10);  // Positionnement du curseur
    M5.Lcd.println("Menu");  // Affichage du titre du menu

    // Crée une file d'attente pour les événements de boutons
    buttonQueue = xQueueCreate(10, sizeof(ButtonEvent));
    if (buttonQueue == NULL) {
        Serial.println("Failed to create queue");
        while (1); // Stop here
    }

    // Crée les tâches
    if (xTaskCreate(checkButtonTask, "Check Button Task", 2048, NULL, 1, NULL) != pdPASS) {
        Serial.println("Failed to create Check Button Task");
        while (1); // Stop here
    }
    if (xTaskCreate(handleButtonEventTask, "Handle Button Event Task", 2048, NULL, 1, NULL) != pdPASS) {
        Serial.println("Failed to create Handle Button Event Task");
        while (1); // Stop here
    }

    // Affichage initial du menu avec la première option sélectionnée
    afficherMenu(option);
}

void loop() {

    static int selectedOptionConfig = 0; // Option actuellement sélectionnée
    static int selectedDigit = 0; // Chiffre actuellement sélectionné

    static int PageSelect = 100;
    static int PageSelectConfig = 4;
    static int gestionCode = 0;

    switch (currentState) {
        case MENU:
            if (stateChanged) {
                afficherMenu(option);
                Serial.println("Affichage du menu principal");
                stateChanged = false;
            }
            break;
        case MENU_CONFIGURATION:
            if (stateChanged) {
                afficherMenuConfig(option);
                Serial.println("Affichage du menu configuration");
                stateChanged = false;
            }
            break;
        case MENU_OUVERTURES:
            if (stateChanged) {
                afficherMenuOuvertures(option);
                Serial.println("Affichage du menu Ouvertures ");

                stateChanged = false;
            }
            break;
        case MENU_ALARME:
             if (stateChanged) {
                afficherMenuAlarme(option);
                Serial.println("Affichage du menu Alarme ");

                stateChanged = false;
            }
            break;
        case MENU_DIGICODE:
            if (stateChanged) {
                afficherDigicode(option);
                Serial.println("Affichage du menu Digicode ");
                stateChanged = false;
            }
            break;
        case MENU_VALIDATION_CODE :
            if (stateChanged) {
                afficherMenuValidationCode(option);
                Serial.println("Affichage du menu Validation Code");
                stateChanged = false;
            }
            break;
        case MENU_SECURITE_CODE :
            if (stateChanged) {
                afficherDigicode(option);
                Serial.println("Affichage du menu Securité Code");
                stateChanged = false;
            }
            break;
            case MENU_CODE :
            if (stateChanged) {
                afficherMenuCode(option);
                Serial.println("Affichage du menu Code");
                stateChanged = false;
            }
            break;
        case MENU_FAIL_CODE :
            if (stateChanged) {
                afficherMenuFailCode(option);
                Serial.println("Affichage du menu Fail Code");
                stateChanged = false;
            }
            break;
        default:
            break;
    }
}

// Fonction pour afficher le menu avec l'option sélectionnée mise en surbrillance
void afficherMenu(int Option) {
    
    M5.Lcd.clear(); // Efface le contenu de l'écran LCD
    M5.Lcd.setTextColor(TFT_WHITE);
    M5.Lcd.setTextSize(5);
    M5.Lcd.setCursor(100, 10);
    M5.Lcd.println("Menu");
    M5.Lcd.setTextSize(3);
    M5.Lcd.setCursor(0, 65); // Déplacement du curseur au début des options
    currentState = MENU;

    if (stateChanged) {
        numOptions = 5; // Nombre total d'options dans le menu
        const char* options[] = {"","1. Configuration", "2. Ouvertures", "3. Alarme", "4. Quitter"};
        for (int i = 0; i < numOptions; ++i) {
            if (i == Option) {
                M5.Lcd.setTextColor(TFT_RED);
            } else {
                M5.Lcd.setTextColor(TFT_WHITE);
            }
            M5.Lcd.println(options[i]);
        }

        switch (selectedOption)
        {
        case 1:
            option = 0;
            selectedOption = 0;
            afficherMenuConfig(option);
            break;
        case 2:
            option = 0;
            selectedOption = 0;
            afficherMenuOuvertures(option);
            return;
            break;
        
        default:
            break;
        }
         stateChanged = false; // Réinitialiser le drapeau de changement d'état
    }
}

// Fonction pour afficher le menu Configuration avec l'option sélectionnée mise en surbrillance
void afficherMenuConfig(int Option) {

    M5.Lcd.clear(); // Efface le contenu de l'écran LCD
    M5.Lcd.setTextColor(TFT_WHITE);
    M5.Lcd.setTextSize(3);
    M5.Lcd.setCursor(65, 10);
    M5.Lcd.println("Configuration");
    M5.Lcd.setTextSize(2);
    currentState = MENU_CONFIGURATION;

    if (stateChanged) {
        numOptions = 4; // Nombre total d'options dans le menu
        const char* options[] = {"  ","1. Changer/Voir le code","2. Ajouter/Supprimer badge", "3. Retour"};

        // Définir les coordonnées x et y pour chaque option
        int x_coords[] = {0,0, 0, 0}; // Positions x des options
        int y_coords[] = {0,70, 100, 130}; // Positions y des options

        for (int i = 0; i < numOptions ; ++i) { 
            M5.Lcd.setCursor(x_coords[i], y_coords[i]);
            if (i == Option) {
                M5.Lcd.setTextColor(TFT_RED);
            } else {
                M5.Lcd.setTextColor(TFT_WHITE);
            }
            M5.Lcd.println(options[i]);
        }

        switch (selectedOption) {
        case 0:
            
            break;
        case 1:
            option = 0;
            selectedOption = 0;
            afficherDigicode (option);
            etape = "menu";
       stateChanged = true;
            break;
        case 2:
        
            break;
        case 3:
            afficherMenu(Option);
            option = 0;
            selectedOption = 0;
            stateChanged = true;
            break;
        default:
            break;
        }

        stateChanged = false; // Réinitialiser le drapeau de changement d'état
    }
}


// Fonction pour afficher le digicode avec le chiffre sélectionné mis en surbrillance
void afficherDigicode (int Option) {

    M5.Lcd.clear(); // Efface le contenu de l'écran LCD
    M5.Lcd.setTextColor(TFT_WHITE);
    M5.Lcd.setCursor(65, 10);
    if (etape == "new"){
        M5.Lcd.setTextSize(2);
        M5.Lcd.println("Tapez le nouveau code");
    }else if (etape == "menu"){
        M5.Lcd.setTextSize(3);
        M5.Lcd.println("Entrez le code");
    }
    M5.Lcd.setTextSize(2);
    M5.Lcd.setCursor(15, 50);
    M5.Lcd.println("mot de passe:");
    M5.Lcd.setCursor(15, 50);
    M5.Lcd.setTextSize(3.5);
    M5.Lcd.drawString(optionHistory, 60, 100);
    M5.Lcd.setTextSize(2);

    currentState = MENU_DIGICODE;

    if (stateChanged) {
        numOptions = 14; // Nombre total d'options dans le menu
        const char* options[] = {"","0","1","2","3","4","5","6","7","8","9","X","Valider","Retour",};

        // Définir les coordonnées x et y pour chaque option
        int x_coords[] = { 0 , 0 , 30 , 60 ,90 , 120 , 150 ,180 , 210 , 240 , 270 , 300 , 50, 170}; // Positions x des options
        int y_coords[] = {0 , 160 , 160 , 160 ,160 , 160 , 160 ,160 , 160 , 160 , 160 , 160 , 200 , 200}; // Positions y des options

        for (int i = 0; i < numOptions ; ++i) { 
            M5.Lcd.setCursor(x_coords[i], y_coords[i]);
            if (i == Option) {
                M5.Lcd.setTextColor(TFT_RED);
            } else {
                M5.Lcd.setTextColor(TFT_WHITE);
            }
            M5.Lcd.println(options[i]);
        }

        switch (selectedOption) {
        case 0:
            
            break;
        case 1:
            selectedOption = 0;
            updateOptionHistory (0);
            afficherDigicode (option);
            stateChanged = true;
            break;
        case 2:
            selectedOption = 0;
            updateOptionHistory (1);
            afficherDigicode (option);
            stateChanged = true;
            break;
        case 3:
            selectedOption = 0;
            updateOptionHistory (2);
            afficherDigicode (option);
            stateChanged = true;
            break;
        case 4:
            selectedOption = 0;
            updateOptionHistory (3);
            afficherDigicode (option);
            stateChanged = true;
            break;
        case 5:
            selectedOption = 0;
            updateOptionHistory (4);
            afficherDigicode (option);
            stateChanged = true;
            break;
        case 6:
            selectedOption = 0;
            updateOptionHistory (5);
            afficherDigicode (option);
            stateChanged = true;
            break;
        case 7:
            selectedOption = 0;
            updateOptionHistory (6);
            afficherDigicode (option);
            stateChanged = true;
            break;
        case 8:
            selectedOption = 0;
            updateOptionHistory (7);
            afficherDigicode (option);
            stateChanged = true;
            break;
        case 9:
            selectedOption = 0;
            updateOptionHistory (8);
            afficherDigicode (option);
            stateChanged = true;
            break;
        case 10:
            selectedOption = 0;
            updateOptionHistory (9);
            afficherDigicode (option);
            stateChanged = true;
            break;
        case 11:
            selectedOption = 0;                                                                                                                         
            retirerDernierCaractere (optionHistory);
            stateChanged = true;
            break;
        case 12:
            option = 0;
            selectedOption = 0;
            if (etape == "menu"){
                if (optionHistory == code){
                    afficherMenuCode (option);
                } else {
                    afficherMenuFailCode (option);
                }
            }
            else if (etape == "new")
            {
                code = optionHistory ;                                                                                                                         
                afficherMenuValidationCode (option);
            }
            stateChanged = true;
            break;
        case 13:
            option = 0;
            selectedOption = 0;
            optionHistory = "";
            afficherMenuConfig(Option);
            stateChanged = true;
            break;
        default:
            break;
        }

        stateChanged = false; // Réinitialiser le drapeau de changement d'état
    }
}

void afficherMenuOuvertures(int Option) {
    M5.Lcd.clear(); // Efface le contenu de l'écran LCD
    M5.Lcd.setTextColor(TFT_WHITE);
    M5.Lcd.setTextSize(3);
    M5.Lcd.setCursor(65, 10);
    M5.Lcd.println("Ouvertures");
    M5.Lcd.setTextSize(2);
    M5.Lcd.setCursor(0, 65); // Déplacement du curseur au début des options

    
}

void afficherMenuAlarme(int Option) {
    M5.Lcd.clear(); // Efface le contenu de l'écran LCD
    M5.Lcd.setTextColor(TFT_WHITE);
    M5.Lcd.setTextSize(3);
    M5.Lcd.setCursor(65, 10);
    M5.Lcd.println("Alarme");
    M5.Lcd.setTextSize(2);
    M5.Lcd.setCursor(0, 65); // Déplacement du curseur au début des options

    
}
/* // Fonction de gestion des boutons
void gestionBoutons() {
    M5.update(); // Mettre à jour l'état des boutons

    if (M5.BtnA.wasReleased() || M5.BtnA.pressedFor(1000, 200)) {
        option = (option - 1 + numOptions ) % numOptions; // Décrémenter l'option avec boucle
        stateChanged = true;
    
    } else if (M5.BtnC.wasReleased() || M5.BtnC.pressedFor(1000, 200)) {
        option = (option + 1); // Incrémenter l'option avec boucle
        stateChanged = true;
    
    } else if (M5.BtnB.wasReleased() || M5.BtnB.pressedFor(1000, 200)) {
        selectedOption = option; // Recopier l'option courante dans selectedOption
        stateChanged = true;
    
    }
} */

// Fonction pour vérifier l'état du bouton
void checkButtonTask(void *pvParameters) {
    ButtonEvent event;
    while (1) {
        M5.update(); // Met à jour l'état des boutons

        event.AwasReleased = M5.BtnA.wasReleased();
        event.BwasReleased = M5.BtnB.wasReleased();
        event.CwasReleased = M5.BtnC.wasReleased();
        
        // Envoie les événements à la file d'attente
        if (xQueueSend(buttonQueue, &event, portMAX_DELAY) != pdPASS) {
            Serial.println("Failed to send to queue");
        }
        
        vTaskDelay(pdMS_TO_TICKS(10)); // Vérifie l'état du bouton toutes les 10 ms
    }
}

// Tâche pour gérer les événements des boutons
void handleButtonEventTask(void *pvParameters) {
    ButtonEvent event;
    while (1) {
        if (xQueueReceive(buttonQueue, &event, portMAX_DELAY) == pdPASS) {
            if (event.AwasReleased) {
                 option = (option - 1 + numOptions ) % numOptions; // Décrémenter l'option avec boucle
                stateChanged = true;
                // Ajoutez ici le code à exécuter lorsque le bouton A est relâché
            } else if (event.BwasReleased) {
                selectedOption = option; // Recopier l'option courante dans selectedOption
                stateChanged = true;
                // Ajoutez ici le code à exécuter lorsque le bouton B est relâché
            } else if (event.CwasReleased) {
                option = (option + 1)% numOptions; // Incrémenter l'option avec boucle
                stateChanged = true;
                // Ajoutez ici le code à exécuter lorsque le bouton C est relâché
            }
        }
    }
}

// Fonction pour détecter les changements et ajouter la valeur de selectedOption à une chaîne de caractères
String updateOptionHistory(int newOption) {
    if (newOption != previousOption) {
        optionHistory += String(newOption) + " ";
        previousOption = -1;
    }
    return optionHistory;
}


// Fonction pour retirer le dernier caractère d'une chaîne de caractères et renvoyer la chaîne modifiée
void retirerDernierCaractere(String valeur) {
    // Vérifier si la chaîne n'est pas vide
    if (valeur.length() > 0) {
        // Retirer le dernier caractère
        valeur.remove(valeur.length() - 2);
        optionHistory = valeur ;
        afficherDigicode (option);
    }
    return;
}

void afficherMenuValidationCode(int Option) {
    
    M5.Lcd.clear(); // Efface le contenu de l'écran LCD
    M5.Lcd.setTextColor(TFT_WHITE);
    M5.Lcd.setTextSize(3);
    M5.Lcd.setCursor(65, 10);
    M5.Lcd.println("Validation ");                                          
    M5.Lcd.setTextSize(2);
    M5.Lcd.setCursor(20 , 100);
    M5.Lcd.println("Votre code est :" );
    M5.Lcd.setCursor(110 , 130);
    M5.Lcd.println( code );
    currentState = MENU_VALIDATION_CODE ;

    if (stateChanged) {
        numOptions = 3; // Nombre total d'options dans le menu
        const char* options[] = {"  "," OK " , "ANNULER"};

        // Définir les coordonnées x et y pour chaque option
        int x_coords[] = {0,70, 150, }; // Positions x des options
        int y_coords[] = {0,190, 190, }; // Positions y des options

        for (int i = 0; i < numOptions ; ++i) { 
            M5.Lcd.setCursor(x_coords[i], y_coords[i]);
            if (i == Option) {
                M5.Lcd.setTextColor(TFT_RED);
            } else {
                M5.Lcd.setTextColor(TFT_WHITE);
            }
            M5.Lcd.println(options[i]);
        }

        switch (selectedOption) {
        case 0:
            
            break;
        case 1:
            option = 0;
            selectedOption = 0;
            optionHistory = "";
            afficherMenu(Option);
            stateChanged = true;
            break;
        case 2:
            option = 0;
            selectedOption = 0;
            afficherDigicode (Option);
            stateChanged = true;
            break;
        default:
            break;
        }

        stateChanged = false; // Réinitialiser le drapeau de changement d'état
    }
}

void afficherMenuFailCode(int Option) {
    
    M5.Lcd.clear(); // Efface le contenu de l'écran LCD
    M5.Lcd.setTextColor(TFT_WHITE);
    M5.Lcd.setTextSize(3);
    M5.Lcd.setCursor(80, 10);
    M5.Lcd.println(" FAIL ");
    M5.Lcd.setTextSize(2);
    M5.Lcd.setCursor(40 , 100);
    M5.Lcd.println("Vous avez tape(e) " );
    M5.Lcd.setCursor(20 , 130);
    M5.Lcd.println("le mauvais mot de passe" );
    currentState = MENU_FAIL_CODE ;

    if (stateChanged) {
        numOptions = 2; // Nombre total d'options dans le menu
        const char* options[] = {"  "," OK " };

        // Définir les coordonnées x et y pour chaque option
        int x_coords[] = {0,100 }; // Positions x des options
        int y_coords[] = {0,190 }; // Positions y des options

        for (int i = 0; i < numOptions ; ++i) { 
            M5.Lcd.setCursor(x_coords[i], y_coords[i]);
            if (i == Option) {
                M5.Lcd.setTextColor(TFT_RED);
            } else {
                M5.Lcd.setTextColor(TFT_WHITE);
            }
            M5.Lcd.println(options[i]);
        }

        switch (selectedOption) {
        case 0:
            
            break;
        case 1:
            option = 0;
            selectedOption = 0;
            optionHistory = "";
            afficherMenuConfig(Option);
            stateChanged = true;
            break;
        default:
            break;
        }

        stateChanged = false; // Réinitialiser le drapeau de changement d'état
    }
}

void afficherMenuCode(int Option) {

    M5.Lcd.clear(); // Efface le contenu de l'écran LCD
    M5.Lcd.setTextColor(TFT_WHITE);
    M5.Lcd.setTextSize(3);
    M5.Lcd.setCursor(65, 10);
    M5.Lcd.println("MENU CODE");
    M5.Lcd.setTextSize(2);
    currentState = MENU_CODE; 

    if (stateChanged) {
        numOptions = 4; // Nombre total d'options dans le menu
        const char* options[] = {"  ","1. Changer le code","2. Voir le code", "3. Retour"};

        // Définir les coordonnées x et y pour chaque option
        int x_coords[] = {0,0, 0, 0}; // Positions x des options
        int y_coords[] = {0,70, 100, 130}; // Positions y des options

        for (int i = 0; i < numOptions ; ++i) { 
            M5.Lcd.setCursor(x_coords[i], y_coords[i]);
            if (i == Option) {
                M5.Lcd.setTextColor(TFT_RED);
            } else {
                M5.Lcd.setTextColor(TFT_WHITE);
            }
            M5.Lcd.println(options[i]);
        }

        switch (selectedOption) {
        case 0:
            
            break;
        case 1:
            option = 0;
            selectedOption = 0;
            etape = "new";
            afficherDigicode (option);
            stateChanged = true;
            break;
        case 2:
            option = 0;
            selectedOption = 0;
            afficherMenuValidationCode (option);
            stateChanged = true;
            break;
        case 3:
            afficherMenu(Option);
            option = 0;
            selectedOption = 0;
            stateChanged = true;
            break;
        default:
            break;
        }

        stateChanged = false; // Réinitialiser le drapeau de changement d'état
    }
}
